#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
	
int
main(int argc, char *argv[])
{
    int k;
    int array[8];
    int i = 0;
    int fd;

    if(argc <= 1 || argc > 9){
        exit();
    }
    while(i < argc-1){
        array[i] = atoi((argv[i+1]));
        i++;
    }
    
    while(i<8){
        array[i] = 1;
        i++;
    }
    k = array[0];
    while (k%array[0]!=0 || k%array[1]!=0 || k%array[2]!=0 || k%array[3]!=0 || k%array[4]!=0 || k%array[5]!=0 || k%array[6]!=0 || k%array[7]!=0)
    {
        k += 1;
    }
    
    fd = open("lcm_result.txt", O_CREATE | O_RDWR);
    printf(fd, "Lcm = %d\n", k);
    close(fd);
    exit();
}